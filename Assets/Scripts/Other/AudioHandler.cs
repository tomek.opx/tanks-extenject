﻿using UnityEngine;

namespace Zenject.Tanks
{
    public class AudioHandler
    {
        readonly Camera _camera;

        public AudioHandler(Camera camera)
        {
            _camera = camera;
        }

        public void Play(AudioClip clip)
        {
            Play(clip, 1);
        }
        public void Play(AudioClip clip, float volume)
        {
            _camera.GetComponent<AudioSource>().PlayOneShot(clip, volume);
        }
    }

}
