﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Zenject.Tanks
{
    public class GameRestartHandler : IInitializable, IDisposable, ITickable
    {
        readonly SignalBus _signalBus;
        readonly Settings _settings;

        bool _isDelaying;
        float _delayStartTime;

        public GameRestartHandler(SignalBus signalBus, Settings settings)
            {
                _signalBus = signalBus;
            _settings = settings;
            }
        public void Dispose()
        {
            _signalBus.Unsubscribe<PlayerDiedSignal>(OnPlayerDied);
        }

        public void Initialize()
        {
            _signalBus.Subscribe<PlayerDiedSignal>(OnPlayerDied);
        }

        private void OnPlayerDied()
        {
            _delayStartTime = Time.realtimeSinceStartup;
            _isDelaying = true;
        }

        public void Tick()
        {
            if (_isDelaying) 
            { 
                if(Time.realtimeSinceStartup - _delayStartTime > _settings.RestartDelay)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
        }
        }
        [Serializable]
        public class Settings
        {
            public float RestartDelay;
        }
    }
    }
    


