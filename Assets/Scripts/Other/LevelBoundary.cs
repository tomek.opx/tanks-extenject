﻿using UnityEngine;

namespace Zenject.Tanks
{
    public class LevelBoundary
    {
        readonly Camera _camera;

        public LevelBoundary(Camera camera)
        {
            _camera = camera;
        }

        public float Bottom
        {
            get { return -ExtentHeight; }
        }
        public float Top
        {
            get { return ExtentHeight; }
        }
        public float Left
        {
            get { return -ExtendWidth; }
        }
        public float Right
        {
            get { return ExtendWidth; }
        }
        public float ExtentHeight
        {
            get { return _camera.orthographicSize; }
        }
        public float ExtendWidth
        {
            get { return _camera.aspect * _camera.orthographicSize; }
        }
        public float Height
        {
            get { return ExtentHeight * 2.0f; }
        }
        public float Width
        {
            get { return ExtendWidth * 2.0f; }
        }
    }

}
