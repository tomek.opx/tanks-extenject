﻿using ModestTree;
using System;
using UnityEngine;

namespace Zenject.Tanks
{
    public class EnemySpawner : ITickable, IInitializable
    {
        readonly EnemyFacade.Factory _enemyFacotry;
        readonly SignalBus _signalBus;
        readonly LevelBoundary _levelBoundary;
        readonly Settings _settings;

        float _desiredNumEnemies;
        float _lastSpawnTime;

        int _enemyCount;

        public EnemySpawner(Settings settings, LevelBoundary levelBoundary, SignalBus signalBus, EnemyFacade.Factory enemyFactory)
        {
            _enemyFacotry = enemyFactory;
            _levelBoundary = levelBoundary;
            _settings = settings;
            _signalBus = signalBus;

            _desiredNumEnemies = settings.NumEnemiesStartAmount;
        }

        public void Initialize()
        {
            _signalBus.Subscribe<EnemyKilledSignal>(OnEnemyKilled);
        }
        void OnEnemyKilled()
        {
            _enemyCount--;
        }
        public void Tick()
        {
            _desiredNumEnemies += _settings.NumEnemiesIncreaseRate * Time.deltaTime;

            if(_enemyCount < (int)_desiredNumEnemies && Time.realtimeSinceStartup - _lastSpawnTime > _settings.MinDelayBetweenSpawns)
            {
                SpawnEnemy();
                _enemyCount++;
            }
        }
        void SpawnEnemy()
        {
            float speed = UnityEngine.Random.Range(_settings.SpeedMin, _settings.SpeedMax);
            float accuracy = UnityEngine.Random.Range(_settings.AccuracyMin, _settings.AccuracyMax);

            var enemyFacade = _enemyFacotry.Create(accuracy, speed);
            enemyFacade.Position = ChoseRandomStartPosition();

            _lastSpawnTime = Time.realtimeSinceStartup;
        }

        Vector3 ChoseRandomStartPosition()
        {
            var side = UnityEngine.Random.Range(0, 3);
            var posOnSide = UnityEngine.Random.Range(0, 1.0f);

            float buffer = 2.0f;

            switch (side)
            {
                case 0:
                    {
                        return new Vector3(_levelBoundary.Left + posOnSide * _levelBoundary.Width, 0, _levelBoundary.Top + buffer );
                    }
                case 1:
                    {
                        return new Vector3(_levelBoundary.Right + buffer, 0,_levelBoundary.Top - posOnSide * _levelBoundary.Height);
                    }
                case 2:
                    {
                        return new Vector3(_levelBoundary.Left + posOnSide * _levelBoundary.Width, 0, _levelBoundary.Bottom - buffer);
                    }
                case 3:
                    {
                        return new Vector3(_levelBoundary.Left - buffer, 0, _levelBoundary.Top - posOnSide * _levelBoundary.Height);
                    }
            }
                    throw Assert.CreateException();

            }
            /*switch (side)
            {
                case 0:
                    {
                        return new Vector3(_levelBoundary.Left + posOnSide * _levelBoundary.Width, _levelBoundary.Top + buffer, 0);
                    }
                case 1:
                    {
                        return new Vector3(_levelBoundary.Right + buffer, _levelBoundary.Top - posOnSide * _levelBoundary.Height, 0);
                    }
                case 2:
                    {
                        return new Vector3(_levelBoundary.Left + posOnSide * _levelBoundary.Width, _levelBoundary.Bottom - buffer, 0);
                    }
                case 3:
                    {
                        return new Vector3(_levelBoundary.Left - buffer, _levelBoundary.Top - posOnSide * _levelBoundary.Height, 0);
                    }
            }
                    throw Assert.CreateException();

            } */

            [Serializable]
            public class Settings
        {
            public float SpeedMin;
            public float SpeedMax;

            public float AccuracyMin;
            public float AccuracyMax;

            public float NumEnemiesIncreaseRate;
            public float NumEnemiesStartAmount;

            public float MinDelayBetweenSpawns = 0.5f;
        }
        }
    }

