﻿using System;

namespace Zenject.Tanks
{
    [Serializable]
    public class EnemyCommonSettings
    {
        public float AttackDistance = 15.0f;
    }

}
