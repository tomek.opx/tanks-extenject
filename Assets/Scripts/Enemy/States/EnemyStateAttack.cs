﻿using System;
using UnityEngine;

namespace Zenject.Tanks
{
    public class EnemyStateAttack : IEnemyState
    {
        readonly EnemyRotationHandler _rotationHandler;
        readonly EnemyCommonSettings _commonSettings;
        readonly EnemyTunables _tunables;
        readonly EnemyStateManager _stateManager;
        readonly PlayerFacade _player;
        readonly Settings _settings;
        readonly EnemyView _view;
        readonly Bullet.Factory _bulletFactory;
        readonly AudioHandler _audioHandler;


        float _lastShootTime;
        bool _strafeRight;
        float _lastStrafeChangeTime;

        public EnemyStateAttack(Bullet.Factory bulletFactory, EnemyView view, Settings settings, PlayerFacade player, EnemyTunables tunables, EnemyStateManager stateManager, EnemyCommonSettings commonSettings, EnemyRotationHandler rotationHandler, AudioHandler audioHandler)
        {
            _rotationHandler = rotationHandler;
            _commonSettings = commonSettings;
            _tunables = tunables;
            _bulletFactory = bulletFactory;
            _settings = settings;
            _player = player;
            _stateManager = stateManager;
            _view = view;
            _audioHandler = audioHandler;

        }
        public void EnterState() { }
    
        public void ExitState() { }

        public void Update()
        {
            if (_player.IsDead)
            {
                return;
            }

            _rotationHandler.DesiredLookDir = (_player.Position - _view.Position).normalized;

            if(Time.realtimeSinceStartup - _lastStrafeChangeTime > _settings.StrafeChangeInterval)
            {
                _lastStrafeChangeTime = Time.realtimeSinceStartup;
                _strafeRight = !_strafeRight;
            }

            // shooting interval
            if(Time.realtimeSinceStartup - _lastShootTime > _settings.ShootInterval)
            {
                _lastShootTime = Time.realtimeSinceStartup;
                Fire();
            }
            if((_player.Position - _view.Position).magnitude > _commonSettings.AttackDistance + _settings.AttackRangeBuffer)
            {
                _stateManager.ChangeState(EnemyStates.Follow);
            }
        }
        public void FixedUpdate()
        {
            if (_strafeRight)
            {
                _view.Addforce(_view.RightDir * _settings.StrafeMultiplier * _tunables.Speed);
            }
            else
            {
                _view.Addforce(-_view.RightDir * _settings.StrafeMultiplier * _tunables.Speed);
            }
        }
        void Fire()
        {
            var bullet = _bulletFactory.Create(_settings.BulletSpeed, _settings.BulletLifetime, BulletTypes.FromEnemy);

            var accuracy = Mathf.Clamp(_tunables.Accuracy, 0, 1);
            var maxError = 1.0f - accuracy;
            var error = UnityEngine.Random.Range(0, maxError);

            if(UnityEngine.Random.Range(0.0f,1.0f) < 0.5f)
            {
                error *= 1;
            }
            var thetaError = error * _settings.ErrorRangeTheta;

            bullet.transform.position = _view.Position + _view.LookDir * _settings.BulletOffsetDistance;
            bullet.transform.rotation = Quaternion.AngleAxis(thetaError, Vector3.forward) * _view.Rotation;

            _audioHandler.Play(_settings.ShootSound, _settings.ShootSoundVol);
        }
        [Serializable]
        public class Settings
        {
            public AudioClip ShootSound;
            public float ShootSoundVol = 1.0f;

            public float BulletLifetime;
            public float BulletSpeed;
            public float BulletOffsetDistance;
            public float ShootInterval;
            public float ErrorRangeTheta;
            public float AttackRangeBuffer;
            public float StrafeMultiplier;
            public float StrafeChangeInterval;
        }
    }


}
