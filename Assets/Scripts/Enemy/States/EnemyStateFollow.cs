﻿using System;
using UnityEngine;

namespace Zenject.Tanks
{
    public class EnemyStateFollow : IEnemyState
    {
        readonly EnemyRotationHandler _rotationHandler;
        readonly EnemyCommonSettings _commonSettings;
        readonly EnemyTunables _tunables;
        readonly EnemyStateManager _stateManager;
        readonly PlayerFacade _player;
        readonly Settings _settings;
        readonly EnemyView _view;
        readonly Bullet.Factory _bulletFactory;

        bool _strafeRight;
        float _lastStrafeChangeTime;

        public EnemyStateFollow(Bullet.Factory bulletFactory, EnemyView view, Settings settings, PlayerFacade player, EnemyTunables tunables, EnemyStateManager stateManager, EnemyCommonSettings commonSettings, EnemyRotationHandler rotationHandler)
        {
            _rotationHandler = rotationHandler;
            _commonSettings = commonSettings;
            _tunables = tunables;
            _bulletFactory = bulletFactory;
            _settings = settings;
            _player = player;
            _stateManager = stateManager;
            _view = view;
        }
        public void EnterState() 
        {
            _strafeRight = UnityEngine.Random.Range(0, 1) == 0;
            _lastStrafeChangeTime = Time.realtimeSinceStartup;
        }

        public void ExitState() { }

        public void Update()
        {
            if (_player.IsDead)
            {
                _stateManager.ChangeState(EnemyStates.Idle);
                return;
            }

            var distanceToPlayer = (_player.Position - _view.Position).magnitude;

            _rotationHandler.DesiredLookDir = (_player.Position - _view.Position).normalized;

            if(Time.realtimeSinceStartup - _lastStrafeChangeTime > _settings.StrafeChangeInterval)
            {
                _lastStrafeChangeTime = Time.realtimeSinceStartup;
                _strafeRight = !_strafeRight;
            }
            if(distanceToPlayer < _commonSettings.AttackDistance)
            {
                _stateManager.ChangeState(EnemyStates.Attack);
            }
        }
        public void FixedUpdate()
        {
            MoveTowardsPlayer();
            Strafe();
        }

        private void Strafe()
        {
            if (_strafeRight)
            {
                _view.Addforce(_view.RightDir * _settings.StrafeMultiplier * _tunables.Speed);
            }
            else
            {
                _view.Addforce(-_view.RightDir * _settings.StrafeMultiplier * _tunables.Speed);
            }
        }

        private void MoveTowardsPlayer()
        {
            var playerDir = (_player.Position - _view.Position).normalized;

            _view.Addforce(playerDir * _tunables.Speed);
        }
        [Serializable]
        public class Settings
        {
            public float StrafeMultiplier;
            public float StrafeChangeInterval;
            public float TeleportNewDistance;
        }
    }
    }
