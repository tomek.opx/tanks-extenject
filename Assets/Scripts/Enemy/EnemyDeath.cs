﻿using System;
using UnityEngine;

namespace Zenject.Tanks
{
public class EnemyDeath 
{
        readonly EnemyFacade _facade;
        readonly SignalBus _signalBus;
        readonly Settings _settings;
        readonly Explosion.Factory _explosionFactory;
        readonly EnemyView _view;
        readonly AudioHandler _audioHandler;

        public EnemyDeath(EnemyView view, Explosion.Factory explosionFactory, Settings settings, SignalBus signalBus, EnemyFacade facade, AudioHandler audioHandler)
        {
            _view = view;
            _settings = settings;
            _signalBus = signalBus;
            _explosionFactory = explosionFactory;
            _facade = facade;
            _audioHandler = audioHandler;
        }
        public void Die()
        {
            var explosion = _explosionFactory.Create();
            explosion.transform.position = _view.Position;

            _audioHandler.Play(_settings.DeathSound, _settings.DeathSoundVol);

            _signalBus.Fire<EnemyKilledSignal>();

            _facade.Dispose();
        }
        [Serializable]
        public class Settings
        {
            public AudioClip DeathSound;
            public float DeathSoundVol = 1.0f;
        }
    }
}
