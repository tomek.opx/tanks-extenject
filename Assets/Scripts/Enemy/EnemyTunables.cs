﻿using System;

namespace Zenject.Tanks
{
        [Serializable]
        public class EnemyTunables
        {
        public float Accuracy;
        public float Speed;
        }
}
