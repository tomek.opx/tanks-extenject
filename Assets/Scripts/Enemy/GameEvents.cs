﻿namespace Zenject.Tanks
{
    public struct PlayerDiedSignal { }
  
    public struct EnemyKilledSignal { }
}