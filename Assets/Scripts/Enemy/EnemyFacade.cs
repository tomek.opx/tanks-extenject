﻿using System;
using UnityEngine;

namespace Zenject.Tanks
{

public class EnemyFacade : MonoBehaviour, IPoolable<float, float, IMemoryPool>, IDisposable
{
        EnemyView _view;
        EnemyTunables _tunables;
        EnemyDeath _enemyDeath;
        EnemyStateManager _stateManager;
        EnemyRegistry _registry;
        IMemoryPool _pool;

        [Inject]
        public void Construct(EnemyView view, EnemyTunables tunables, EnemyDeath enemyDeath, EnemyStateManager stateManager, EnemyRegistry registry)
        {
            _view = view;
            _tunables = tunables;
            _enemyDeath = enemyDeath;
            _stateManager = stateManager;
            _registry = registry;
        }

        public EnemyStates State
        {
            get { return _stateManager.CurrentState; }
        }
        public float Accuracy
        {
            get { return _tunables.Accuracy; }
        }
        public float Speed
        {
            get { return _tunables.Speed; }
        }
        public Vector3 Position
        {
            get { return _view.Position; }
            set { _view.Position = value; }
        }
        public void Dispose()
        {
            _pool.Despawn(this);
        }
        public void Die()
        {
            _enemyDeath.Die();
        }
        public void OnDespawned()
        {
            _registry.RemoveEnemy(this);
            _pool = null;
        }
        public void OnSpawned(float accuracy, float speed , IMemoryPool pool)
        {
            _pool = pool;
            _tunables.Accuracy = accuracy;
            _tunables.Speed = speed;

            _registry.AddEnemy(this);
        }
        public class Factory : PlaceholderFactory<float, float, EnemyFacade>
        {
        }
}
}

