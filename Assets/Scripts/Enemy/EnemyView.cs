﻿using UnityEngine;
namespace Zenject.Tanks
{ 
public class EnemyView : MonoBehaviour
    {
        [SerializeField]
        MeshRenderer _renderer = null;

        [SerializeField]
        Collider _collider = null;

        [SerializeField]
        Rigidbody _rigidbody = null;

        [Inject]
        public EnemyFacade Facade
        {
            get;
            set;
        }

        public MeshRenderer Renderer
        {
            get { return _renderer; }
        }
        public Collider Collider
        {
            get { return _collider; }
        }
        public Vector3 LookDir
        {
            get { return _rigidbody.transform.up; }
        }
        public Vector3 RightDir
        {
            get { return _rigidbody.transform.up; }
        }
        public Vector3 ForwardDir
        {
            get { return _rigidbody.transform.right; }
        }
        public Vector3 Position
        {
            get { return _rigidbody.transform.position; }
            set { _rigidbody.transform.position = value; }
        }

        public Quaternion Rotation
        {
            get { return _rigidbody.rotation; }
            set { _rigidbody.rotation = value; }
        }
        public Vector3 Velocity
        {
            get { return _rigidbody.velocity; }
        }
        public Vector3 AngularVelocity
        {
            get { return _rigidbody.angularVelocity; }
            set { _rigidbody.angularVelocity = value; }
        }
        public void Addforce(Vector3 force)
        {
            _rigidbody.AddForce(force);
        }
        public void AddTorque(float value)
        {
            _rigidbody.AddTorque(Vector3.forward * value);
        }
}
}