﻿using UnityEngine;

namespace Zenject.Tanks
{
    public class PlayerFacade : MonoBehaviour
    {
        Player _model;
        PlayerDamage _hitHandler;

        [Inject]
        public void Construct(Player player, PlayerDamage damageHandler)
        {
            _model = player;
            _hitHandler = damageHandler;
        }

        public bool IsDead
        {
            get { return _model.IsDead; }
        }

        public Vector3 Position
        {
            get { return _model.Position; }
        }

        public Quaternion Rotation
        {
            get { return _model.Rotation; }
        }

        public void TakeDamage(Vector3 moveDirection)
        {
            _hitHandler.TakeDamage(moveDirection);
        }
    }
}