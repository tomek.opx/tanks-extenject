﻿using System;
using UnityEngine;

namespace Zenject.Tanks
{
    public class PlayerHealthWatcher : ITickable
    {
        readonly SignalBus _signalBus;
        readonly AudioHandler _audioHandler;
        readonly Settings _settings;
        readonly Explosion.Factory _explosionFactory;
        readonly Player _player;

        public PlayerHealthWatcher(Player player, Explosion.Factory explosionFactory, Settings settings, AudioHandler audioHandler, SignalBus signalBus)
        {
            _signalBus = signalBus;
            _audioHandler = audioHandler;
            _settings = settings;
            _explosionFactory = explosionFactory;
            _player = player;
        }
        public void Tick()
        {
            if (_player.Health <= 0 && !_player.IsDead)
            {
                Die();
            }
        }
        void Die()
        {
            _player.IsDead = true;

            var explosion = _explosionFactory.Create();
            explosion.transform.position = _player.Position;

            _player.Renderer.enabled = false;

            _signalBus.Fire<PlayerDiedSignal>();

            _audioHandler.Play(_settings.DeathSound, _settings.DeathSoundVol);
        }
        [Serializable]
        public class Settings
        {
            public AudioClip DeathSound;
            public float DeathSoundVol = 1.0f;

        }

    }
}
