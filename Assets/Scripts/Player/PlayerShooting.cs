﻿using System;
using UnityEngine;

namespace Zenject.Tanks
{


public class PlayerShooting : ITickable
{
        readonly Player _player;
        readonly Settings _settings;
        readonly PlayerInputState _inputState;
        readonly Bullet.Factory _bulletFactory;
        readonly AudioHandler _audioHandler;

        float _lastFireTime;

        public PlayerShooting( PlayerInputState inputState, Settings settings, Player player, AudioHandler audioHandler, Bullet.Factory factory)
        {
            _inputState = inputState;
            _player = player;
            _settings = settings; _audioHandler = audioHandler;
            _bulletFactory = factory;
        }
        public void Tick()
        {
            if (_player.IsDead)
            {
                return;
            }
            if(_inputState.IsFiring && Time.realtimeSinceStartup - _lastFireTime > _settings.MaxShootInterval)
            {
                _lastFireTime = Time.realtimeSinceStartup;
                Fire();
            }
        }

        void Fire()
        {
            _audioHandler.Play(_settings.ShootSound, _settings.ShootSoundVol);

            var bullet = _bulletFactory.Create(_settings.BulletSpeed, _settings.BulletLifetime, BulletTypes.FromPlayer);

            bullet.transform.position = _player.Position + _player.LookDir * _settings.BulletOffsetDistance;
            bullet.transform.rotation = _player.Rotation;
        }

        [Serializable]
        public class Settings
        {
            public float BulletLifetime;
            public float BulletSpeed;
            public float MaxShootInterval;
            public float BulletOffsetDistance;

            public AudioClip ShootSound;
            public float ShootSoundVol = 1.0f;

        }
    }
}
