﻿using UnityEngine;

namespace Zenject.Tanks
{

public class PlayerDirection : ITickable
{
        readonly Player _player;
        readonly Camera _mainCam;

        public PlayerDirection( Camera camera, Player player)
        {
            _player = player;
            _mainCam = camera;
        }
        public void Tick()
        {
            var mouseRay = _mainCam.ScreenPointToRay(Input.mousePosition);

            var mousePos = mouseRay.origin;
            mousePos.z = 0;

            var goalDir = mousePos - _player.Position;
            goalDir.z = 0;
            goalDir.Normalize();

            _player.Rotation = Quaternion.LookRotation(goalDir) * Quaternion.AngleAxis(90, Vector3.up);
        }
}
}
