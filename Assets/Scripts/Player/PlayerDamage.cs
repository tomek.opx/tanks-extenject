﻿using System;
using UnityEngine;

namespace Zenject.Tanks
{

    public class PlayerDamage
{
        readonly Player _player;
        readonly Settings _settings;
        readonly AudioHandler _audioHandler;

        public PlayerDamage(Player player, Settings settings, AudioHandler audioHandler)
        {
            _player = player;
            _settings = settings;
            _audioHandler = audioHandler;

        }
        public void TakeDamage(Vector3 moveDirection)
        {
            _audioHandler.Play(_settings.DeathSound, _settings.DeathSoundVol);

            _player.TakeDamage(_settings.EnemyDamage);

            _player.AddForce(-moveDirection * _settings.HitForce);
        }

        [Serializable]
    public class Settings
    {
            public float EnemyDamage;
            public float HitForce;

            public AudioClip DeathSound;
            public float DeathSoundVol = 1.0f;
        }

    }
}
