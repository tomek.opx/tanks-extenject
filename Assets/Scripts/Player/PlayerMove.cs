﻿using System;
using UnityEngine;
namespace Zenject.Tanks
{ 
public class PlayerMove : IFixedTickable
{
        readonly PlayerInputState _inputState;
        readonly Player _player;
        readonly Settings _settings;
        readonly LevelBoundary _levelBoundary;

        public PlayerMove(PlayerInputState inputState, Player player, LevelBoundary levelBoundary, Settings settings)
        {
            _inputState = inputState;
            _player = player;
            _settings = settings;
            _levelBoundary = levelBoundary;
        }
        public void FixedTick()
        {
            if (_player.IsDead)
            {
                return;
            }
            if (_inputState.IsMovingLeft)
            {
                _player.AddForce(Vector3.left * _settings.MoveSpeed);
            }
            if (_inputState.IsMovingRight)
            {
                _player.AddForce(Vector3.right * _settings.MoveSpeed);
            }
            if (_inputState.IsMovingUp)
            {
                _player.AddForce(Vector3.up * _settings.MoveSpeed);
            }
            if (_inputState.IsMovingDown)
            {
                _player.AddForce(Vector3.down * _settings.MoveSpeed);
            }
            KeepOnScreen();
        }
        void KeepOnScreen()
        {
            var extentLeft = (_levelBoundary.Left + _settings.BoundaryBuffer) - _player.Position.x;
            var extentRight = _player.Position.x - (_levelBoundary.Right - _settings.BoundaryBuffer);

            if (extentLeft > 0)
            {
                _player.AddForce(
                    Vector3.right * _settings.BoundaryAdjustForce * extentLeft);
            }
            else if (extentRight > 0)
            {
                _player.AddForce(
                    Vector3.left * _settings.BoundaryAdjustForce * extentRight);
            }

            var extentTop = _player.Position.y - (_levelBoundary.Top - _settings.BoundaryBuffer);
            var extentBottom = (_levelBoundary.Bottom + _settings.BoundaryBuffer) - _player.Position.y;

            if (extentTop > 0)
            {
                _player.AddForce(
                    Vector3.down * _settings.BoundaryAdjustForce * extentTop);
            }
            else if (extentBottom > 0)
            {
                _player.AddForce(
                    Vector3.up * _settings.BoundaryAdjustForce * extentBottom);
            }
        }
        [Serializable]
        public class Settings
        {
            public float BoundaryBuffer;
            public float BoundaryAdjustForce;
            public float MoveSpeed;
            public float SlowDownSpeed;
        }
}
}
