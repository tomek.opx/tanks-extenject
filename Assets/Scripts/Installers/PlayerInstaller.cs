﻿using System;
using UnityEngine;

namespace Zenject.Tanks
{
    public class PlayerInstaller : MonoInstaller
    {
        [SerializeField]
        Settings _settings = null;

        public override void InstallBindings()
        {
            Container.Bind<Player>().AsSingle().WithArguments(_settings.Rigidbody, _settings.MeshRenderer);

            Container.BindInterfacesTo<PlayerInputHandler>().AsSingle();
            Container.BindInterfacesTo<PlayerDirection>().AsSingle();
            Container.BindInterfacesTo<PlayerMove>().AsSingle();
            Container.BindInterfacesTo<PlayerShooting>().AsSingle();
            Container.BindInterfacesTo<PlayerHealthWatcher>().AsSingle();
            Container.BindInterfacesAndSelfTo<PlayerDamage>().AsSingle();

            Container.Bind<PlayerInputState>().AsSingle();
        }
        [Serializable]
        public class Settings
        {
            public Rigidbody Rigidbody;
            public MeshRenderer MeshRenderer;
        }
    }

}
