﻿using UnityEngine;

namespace Zenject.Tanks
{
    public class PlayerDiedSignalOBserver
    {
        public void OnPlayerDied()
        {
            Debug.Log("fired playerDiedSignal");
        }
    }

    public class GameSignalInstaller : Installer<GameSignalInstaller>
    {
        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);

            Container.DeclareSignal<EnemyKilledSignal>();
            Container.DeclareSignal<PlayerDiedSignal>();

            Container.BindSignal<PlayerDiedSignal>().ToMethod<PlayerDiedSignalOBserver>(x => x.OnPlayerDied).FromNew();
            Container.BindSignal<EnemyKilledSignal>().ToMethod(() => Debug.Log("Fired enemyKilledSignal"));
        }
    }
}
