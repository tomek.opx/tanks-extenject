﻿using System;
using UnityEngine;

namespace Zenject.Tanks
{ 
    [CreateAssetMenu(menuName = "Tanks/Game Settings")]
    public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
    {
    public EnemySpawner.Settings EnemySpawner;
    public GameRestartHandler.Settings GameRestartHandler;
    public GameInstaller.Settings GameInstaller;
    public PlayerSettings Player;
    public EnemySettings Enemy;

    [Serializable]
    public class PlayerSettings
    {
        public PlayerMove.Settings PlayerMove;
        public PlayerShooting.Settings PlayerShooting;
        public PlayerDamage.Settings PlayerDamage;
        public PlayerHealthWatcher.Settings PlayerHealthWatcher;
    }

    [Serializable]
    public class EnemySettings
    {
        public EnemyTunables DefaultSettings;
        public EnemyStateIdle.Settings EnemyStateIdle;
        public EnemyStateFollow.Settings EnemyStateFollow;
        public EnemyStateAttack.Settings EnemyStateAttack;
        public EnemyDeath.Settings EnemyDeath;
        public EnemyCommonSettings EnemyCommonSettings;
        public EnemyRotationHandler.Settings EnemyRotation;

    }
    public override void InstallBindings()
    {
        Container.BindInstance(EnemySpawner).IfNotBound();
        Container.BindInstance(GameInstaller).IfNotBound();
        Container.BindInstance(GameRestartHandler).IfNotBound();

        Container.BindInstance(Player.PlayerMove).IfNotBound();
        Container.BindInstance(Player.PlayerShooting).IfNotBound();
        Container.BindInstance(Player.PlayerHealthWatcher).IfNotBound();
        Container.BindInstance(Player.PlayerDamage).IfNotBound();

        Container.BindInstance(Enemy.EnemyDeath).IfNotBound();
        Container.BindInstance(Enemy.EnemyStateAttack).IfNotBound();
        Container.BindInstance(Enemy.EnemyStateFollow).IfNotBound();
        Container.BindInstance(Enemy.EnemyStateIdle).IfNotBound();
        Container.BindInstance(Enemy.EnemyCommonSettings).IfNotBound();
        Container.BindInstance(Enemy.EnemyRotation).IfNotBound();
    }
    }

}
