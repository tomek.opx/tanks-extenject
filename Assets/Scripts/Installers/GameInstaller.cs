using UnityEngine;
using System;

namespace Zenject.Tanks
{
    public class GameInstaller : MonoInstaller
    {
        [Inject]
        Settings _settings = null;
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<EnemySpawner>().AsSingle();

            Container.BindFactory<float, float, EnemyFacade, EnemyFacade.Factory>().FromPoolableMemoryPool<float, float, EnemyFacade, EnemyFacadePool>(poolBinder => poolBinder.WithInitialSize(5).FromSubContainerResolve().ByNewPrefabInstaller<EnemyInstaller>(_settings.EnemyFacadePrefab).UnderTransformGroup("Enemies"));

            Container.BindFactory<float, float, BulletTypes, Bullet, Bullet.Factory>().FromPoolableMemoryPool<float, float, BulletTypes, Bullet, BulletPool>(poolBinder => poolBinder.WithInitialSize(20).FromComponentInNewPrefab(_settings.BulletPrefab).UnderTransformGroup("Bullets"));

            Container.Bind<LevelBoundary>().AsSingle();

            Container.BindFactory<Explosion, Explosion.Factory>().FromPoolableMemoryPool<Explosion, ExplosionPool>(MemoryPoolExpandBinder => MemoryPoolExpandBinder.WithInitialSize(4).FromNewComponentOnNewPrefab(_settings.ExplosionPrefab).UnderTransformGroup("Explosions"));

            Container.BindInterfacesTo<GameRestartHandler>().AsSingle();

            Container.Bind<EnemyRegistry>().AsSingle();

            Container.Bind<AudioHandler>().AsSingle();


            GameSignalInstaller.Install(Container);
        }
        [Serializable]
        public class Settings
        {
            public GameObject EnemyFacadePrefab;
            public GameObject BulletPrefab;
            public GameObject ExplosionPrefab;
        }

        class EnemyFacadePool : MonoPoolableMemoryPool<float, float, IMemoryPool, EnemyFacade> { }

        class BulletPool : MonoPoolableMemoryPool<float, float, BulletTypes, IMemoryPool, Bullet> { }

        class ExplosionPool : MonoPoolableMemoryPool<IMemoryPool, Explosion> { }
    }

}