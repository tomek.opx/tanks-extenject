using UnityEngine;
namespace Zenject.Tests.AutoLoadSceneTests
{
    public class Scene1Installer : MonoInstaller<Scene1Installer>
    {
        [SerializeField]
        private GameObject _tank;

        public override void InstallBindings()
        {
            Container.BindInstance(_tank);
            Container.Bind<Qux>().AsSingle();
        }
    }
}
